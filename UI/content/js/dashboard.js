/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 6;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "KO",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "OK",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9523809523809523, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "login.pl"], "isController": false}, {"data": [1.0, 500, 1500, "home.html"], "isController": false}, {"data": [1.0, 500, 1500, "nav.pl"], "isController": false}, {"data": [0.0, 500, 1500, "Test"], "isController": true}, {"data": [1.0, 500, 1500, "reservations.pl"], "isController": false}, {"data": [1.0, 500, 1500, "itinerary.pl"], "isController": false}, {"data": [1.0, 500, 1500, "itinerary"], "isController": false}, {"data": [1.0, 500, 1500, "signOff"], "isController": false}, {"data": [1.0, 500, 1500, "welcome.pl"], "isController": false}, {"data": [1.0, 500, 1500, "flights"], "isController": false}, {"data": [1.0, 500, 1500, "WebTours"], "isController": false}, {"data": [1.0, 500, 1500, "WebTours\/header"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 4000, 0, 0.0, 196.08000000000013, 1, 456, 293.0, 317.9499999999998, 364.0, 98.5731536016166, 155.20992808780403, 46.081024051849475], "isController": false}, "titles": ["Label", "#Samples", "KO", "Error %", "Average", "Min", "Max", "90th pct", "95th pct", "99th pct", "Transactions\/s", "Received", "Sent"], "items": [{"data": ["login.pl", 400, 0, 0.0, 221.39499999999987, 111, 385, 271.90000000000003, 290.0, 357.93000000000006, 10.698906036857732, 11.43604916983176, 5.5166234252547675], "isController": false}, {"data": ["home.html", 400, 0, 0.0, 40.67249999999995, 2, 58, 48.0, 49.0, 55.99000000000001, 10.11582621010571, 9.148821387701684, 4.47506764958778], "isController": false}, {"data": ["nav.pl", 800, 0, 0.0, 234.06874999999977, 88, 408, 279.0, 298.0, 326.0, 19.78973407544836, 33.861800401978975, 7.986433209647496], "isController": false}, {"data": ["Test", 200, 0, 0.0, 3921.6050000000014, 3399, 4364, 4136.5, 4180.75, 4323.700000000001, 4.919807143559972, 154.93121302457445, 45.99827499262029], "isController": true}, {"data": ["reservations.pl", 800, 0, 0.0, 278.88500000000005, 129, 456, 339.0, 357.89999999999986, 395.99, 21.125458818558716, 66.59808628924714, 13.806848939766036], "isController": false}, {"data": ["itinerary.pl", 200, 0, 0.0, 232.33000000000004, 99, 350, 275.9, 286.95, 348.6600000000003, 5.4547933996999864, 6.14758945008864, 2.15741340515478], "isController": false}, {"data": ["itinerary", 200, 0, 0.0, 222.6000000000001, 111, 341, 268.9, 285.84999999999997, 316.98, 5.424758598242378, 4.333979501193447, 2.3203557285450795], "isController": false}, {"data": ["signOff", 200, 0, 0.0, 223.23000000000005, 93, 343, 258.9, 277.0, 313.0, 5.4709084443471845, 5.476758683357496, 2.3133821839866506], "isController": false}, {"data": ["welcome.pl", 400, 0, 0.0, 209.26000000000016, 54, 371, 266.0, 284.74999999999994, 311.0, 10.355450850441402, 9.445683030975742, 4.161394555621715], "isController": false}, {"data": ["flights", 200, 0, 0.0, 241.01, 141, 423, 301.9, 341.9, 394.97, 5.413452429286778, 9.246668400663149, 2.220361347949655], "isController": false}, {"data": ["WebTours", 200, 0, 0.0, 4.719999999999998, 1, 40, 8.0, 16.94999999999999, 39.0, 5.380249105533586, 3.4842104400371237, 2.02810171360934], "isController": false}, {"data": ["WebTours\/header", 200, 0, 0.0, 3.240000000000001, 1, 37, 4.0, 7.0, 33.80000000000018, 5.384304751648943, 5.294916879795396, 1.9823075111051285], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Percentile 1
            case 8:
            // Percentile 2
            case 9:
            // Percentile 3
            case 10:
            // Throughput
            case 11:
            // Kbytes/s
            case 12:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 4000, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
